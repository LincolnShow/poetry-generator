#!perl
use strict;
use warnings;
use utf8;
use Encode;
use List::Util qw(shuffle);
binmode STDOUT, ":utf8";
open(UNF, "<:encoding(UTF-8)", "unf.txt") or die "Yay!";
my @unions = <UNF>;
chomp @unions;
close(UNF) or die "Oh!";

my $i = 0;
foreach my $u(@unions){
	if($u =~ /\A\W*\Z/){
		splice (@unions, $i, 1);
	}
	$i++;
}
$i = 0;
foreach my $u(@unions){
	if($u =~ /\A\W*\Z/){
		splice (@unions, $i, 1);
	}
	$i++;
}
my @result;
my $n = 2;
foreach my $u(@unions){
	if($n == 2){
		push (@result, lc $u);
		$n = -1;
	}
	$n++;
}
open(FORM, ">:encoding(UTF-8)", "unions.txt") or die "Ouch!";
foreach my $r(@result){
	print FORM $r,"\n";
}
close(FORM) or die "No!";
#print join("\n", @unions);
#print join("\n", @result);