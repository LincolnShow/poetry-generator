﻿#!perl
use strict;
use warnings;
use utf8;
use Encode;
use List::Util qw(shuffle);
binmode STDOUT, ":utf8";

open(DICTIONARY, "<:encoding(UTF-8)", "dic2.txt") or die "Couldn't open the file!";
open(ENDINGS, "<:encoding(UTF-8)", "endings.txt") or die "Couldn't open the file!";
open(UNIONS, "<:encoding(UTF-8)", "unions.txt") or die "Couldn't open the file!";
my @words = shuffle <DICTIONARY>;
my @endings = <ENDINGS>;
my @unions = <UNIONS>;
chomp @words;
chomp @endings;
chomp @unions;
close(DICTIONARY) or die "Couldn't close the file!";
close(ENDINGS) or die "Couldn't close the file!";
close(UNIONS) or die "Couldn't close the file!";

my @data;
my $size = shift;
my $step = shift;
my $lastrhyme = "";
my $minWords = 3;
my $maxWords = 4;
for(my $lines = 0; $lines < $size; $lines++){
	my $words = int(rand($maxWords-$minWords+1)+$minWords);
	$data[$lines] = "";
	while($words > 1){
		if(int(rand(2)) == 1){
			my $u = $unions[int(rand(0+@unions))];
			my $word = $words[int(rand(0+@words))];
			if($u =~ /X/){
				$u =~ s/X/$word/;
				$data[$lines] = $data[$lines] . " " . $u;
			}else{
				$data[$lines] = $data[$lines] . " " . $word;
				$data[$lines] = $data[$lines] . " " . $u;
			}
		} else {
			$data[$lines] = $data[$lines] . " " . $words[int(rand(0+@words))];
		}
		$words--;
	}
	if($lastrhyme eq ""){
		my $lastword = $words[int(rand(0+@words))];
		$lastrhyme = getLastNLetters($lastword, 4-int(rand(1)));
		$data[$lines] = $data[$lines] . " " . $lastword;
	} else {
		$data[$lines] = $data[$lines] . " " . getRhyme($lastrhyme, @words);
		$lastrhyme = "";
	}
}
for(my $i = 0; $i < 0+@data; $i++){
	print $data[$i], "\n";
	if ((($i+1) % $step) == 0){
		print "\n";
	}
}

sub getLastNLetters{
	my $word = shift;
	my $N = shift;
	if(length($word) < $N){
		return $word;
	}
	return substr($word, -$N);
}
sub getRhyme{
	my $lastrhyme = shift;
	my @words = @_;
	@words = shuffle @words;
	foreach my $word(@words){
		if($word =~ /\w$lastrhyme\Z/){
			return $word;
		}
	}
	return $lastrhyme;
}